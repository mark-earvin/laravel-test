<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
// use DB;
use App\Card;

class CardsController extends Controller
{
    public function index(){
        $cards = Card::all();
        return view('cards.index', compact('cards'));
    }

    public function show(Card $card){
        // $card = Card::with('notes')->find(1);
        // return $card;

        $card->load('notes.user');
        // return $card;

        return view('cards.show', compact('card'));
    }
}
