<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Note;
use App\Card;

class NotesController extends Controller
{
    //
    public function store(Request $request){
        $card = Card::find($request->card_id);

        $this->validate($request, [
            "body" => "required|min:10"
        ]);

        // $note = new Note($request->all());
        // $note->by(Auth::user());

        $card->addNote(new Note(['body'=>$request->body]));

        return back();
    }

    public function edit(Note $note){
        return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note){
        $note->update($request->all());
        // return back();
        // return redirect('/cards/' + $note->card()->id);
        return $note->card();
    }
}
