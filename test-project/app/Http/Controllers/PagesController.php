<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    //
    public function home(){
        $people = ['mark', 'jas', 'rich', 'jara', 'abe'];
        return view('welcome', [
            'people' => $people,
        ]);
    }

    public function about(){
        return view('pages.about');
    }
}
