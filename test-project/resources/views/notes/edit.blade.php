@extends('layout')

@section('content')
    <div class="row">
        <h3>Edit Note</h3>
        <form method="post" action="/notes/{{ $note->id }}">
            {{ method_field('PATCH') }}
            <div class="form-group">
                <textarea name="body" class="form-control">{{ $note->body }}</textarea>
            </div>

            <div class="form-group" hidden>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update Note</button>
            </div>
        </form>
    </div>
@stop
