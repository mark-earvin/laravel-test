@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>{{ $card->title }}</h2>
            <ul class="list-group">
                @foreach ($card->notes as $note)
                    <li class="list-group-item"><a href="/notes/{{ $note->id }}/edit"><span class="btn glyphicon glyphicon-pencil"></span></a> &nbsp; {{ $note->body }} <a href="#" class="pull-right">{{ $note->user->username }}</a>
                    </li>
                @endforeach
            </ul>

            <hr/>

            <h3>Add a new Note</h3>
            <form method="post" action="/notes">
                <div class="form-group">
                    <textarea name="body" class="form-control">{{ old('body') }}</textarea>
                </div>

                <div class="form-group" hidden>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>

                <div class="form-group" hidden>
                    <input type="hidden" name="card_id" value="{{ $card->id }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Add Note</button>
                </div>
            </form>

            @if (count($errors))
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                    @endforeach
                </ul>
            @endif

        </div>
    </div>
@stop
