@extends('layout')

@section('content')
    <h2>All Cards</h2>
    @foreach ($cards as $card)
        <div>
            <!-- <a href="/cards/{{ $card->id }}">{{$card->title}}</a> -->
            <a href="{{ $card->path() }}">{{$card->title}}</a>
        </div>
    @endforeach
@stop
