@extends('layout')

@section('content')
        @if (empty($people))
            <h3>There are no people.</h3>
        @else
            <h3>Some people here.</h3>
        @endif
        <ul>
        @foreach ($people as $person)
            <li>{{$person}}</li>
        @endforeach
        </ul>
@stop
