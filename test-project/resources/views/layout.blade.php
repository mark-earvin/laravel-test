<!DOCTYPE html>
<html>
    <head>
        <title>My Test Laravel Project</title>

        <!-- <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css"> -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />
    </head>
    <body>

    <div class="container">
        @yield('content')
    </div>

    </body>
</html>
